package Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.garciade.tappunch.API;
import com.example.garciade.tappunch.CustomAdapter;
import com.example.garciade.tappunch.FragmentRankingActivity;
import com.example.garciade.tappunch.Level1;
import com.example.garciade.tappunch.Menu;
import com.example.garciade.tappunch.R;
import com.example.garciade.tappunch.Ranking;
import com.example.garciade.tappunch.UserInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.google.android.gms.internal.zzagz.runOnUiThread;

/**
 * Created by Aitor on 27/05/2018.
 */

public class base_fragment extends Fragment {

    // RANKING VARIABLES
    private String TAG = Ranking.class.getSimpleName();

    private ProgressDialog pDialog;
    private ListView lv;
    List<UserInfo> users_list;

    public String url = "https://api.myjson.com/bins/1b7sd2";
    //public String url = "https://api.myjson.com/bins/1830y6";


    /**
     * Instanciates layout xml
     * @param inflater
     * @param container
     * @param savedInstaceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstaceState){

        users_list = new ArrayList<>();
        new GetRanking().execute();

        return inflater.inflate(R.layout.score_fragment_layout, container, false);

    }

    /**
     * Sets button functionality
     */
    @Override
    public void onStart() {

        super.onStart();

        ImageButton backButton = (ImageButton)getActivity().findViewById(R.id.back_button);
        backButton.setOnClickListener( new View.OnClickListener() {

            public void onClick(View view) {

                Intent myIntent = new Intent(getActivity(), Menu.class);
                startActivity(myIntent);

            }
        });
    }

    /**
     * Parses JSON
     */
    // NEW RANKING CLASS
    private class GetRanking extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            this.showLoader();
        }

        private void showLoader(){
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Retrieving data...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        private void hideLoader(){
            if(pDialog.isShowing()){
                pDialog.dismiss();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            API api = new API();

            String jsonStr = api.getJSON(url);
            Log.e(TAG, "Responde " + jsonStr);

            if(jsonStr != null){

                try{
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    JSONArray users = jsonObj.getJSONArray("Ranking");

                    for(int i = 0; i < users.length(); i++){
                        JSONObject c = users.getJSONObject(i);

                        String name = c.getString("name");
                        String score1 = c.getString("score1");
                        String score2 = c.getString("score2");


                        HashMap<String, String> user = new HashMap<>();
                        user.put("name", name);
                        user.put("score1", score1);
                        user.put("score2", score2);

                        UserInfo current_user = new UserInfo(name,  "Highscore 1:   " + score1,  "Highscore 2:   " + score2);

                        users_list.add(current_user);
                    }
                    // Add this player

                } catch (final JSONException e){

                    Log.e(TAG, "Error: " + e.getMessage());
                    runOnUiThread(() -> {
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Error: " + e.getMessage(),
                                Toast.LENGTH_LONG)
                                .show();

                    });
                }
            } else{
                Log.e(TAG, "Server Error");
                runOnUiThread(new Runnable(){
                    @Override
                    public void run(){
                        Toast.makeText(getActivity().getApplicationContext(),
                                "ERROR!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });
            }

            return null;
        }

        /**
         * Delete pDialog and uses adapter to fill the list view
         * @param result
         */
        @Override
        protected void onPostExecute(Void result){
            super.onPostExecute(result);

            this.hideLoader();

            lv = (ListView) getActivity().findViewById(R.id.list);

            String[] usersArray = new String[]{"name", "class", "primary"};

            CustomAdapter adapter = new CustomAdapter(getActivity().getApplicationContext(), users_list);
            lv.setAdapter(adapter);

        }
    }
}