package com.example.garciade.tappunch;


import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.ContentValues.TAG;

/**
 * Created by Aitor on 26/05/2018.
 */

public class API {


    /**
     * Dowloads the JSON from the given URL
     * @param request
     * @return
     */
    public String getJSON(String request){
        String response = null;
        try{
            URL url = new URL(request);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            InputStream inputStream = new BufferedInputStream(conn.getInputStream());
            response = streamToString(inputStream);

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return response;
    }

    /**
     * Reads the downloaded JSON
     * @param inputStream
     * @return
     */
    private String streamToString(InputStream inputStream){

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();

        String line;
        try{
            while((line = reader.readLine()) != null){
                stringBuilder.append(line).append('\n');
            }
        } catch(IOException e){
            e.printStackTrace();
        }finally{
            try{
                inputStream.close();
            } catch(IOException e){
                e.printStackTrace();
            }
        }

        return stringBuilder.toString();

    }


}