package com.example.garciade.tappunch;

import android.app.Application;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by Aitor on 28/05/2018.
 */

public class AnalyticsEvents {

    static private FirebaseAnalytics mFirebaseAnalytics;

    /**
     * Initializes the Firebase wrapper
     * @param app
     */
    public static void init(Application app){

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(app);

    }

    /**
     * Notifies Firebase an event on the app
     * @param event_id
     * @param event_name
     * @param content_type
     */
    static public void notifyEvent(String event_id, String event_name, String content_type){

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, event_id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_id);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, content_type);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

    }

}
