package com.example.garciade.tappunch;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Contact extends AppCompatActivity {

    /**
     * Creates the contact activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        ImageButton backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener( new View.OnClickListener() {

            public void onClick(View view) {

                Intent myIntent = new Intent(Contact.this, Menu.class);
                startActivity(myIntent);

            }
        });

    }
}