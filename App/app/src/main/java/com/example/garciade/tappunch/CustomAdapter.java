package com.example.garciade.tappunch;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Aitor on 27/05/2018.
 */

public class CustomAdapter extends BaseAdapter {

    public Context context;
    public List<UserInfo> UsersInfoList;


    public CustomAdapter(Context Context, List<UserInfo> dataList){

        this.context = Context;
        this.UsersInfoList = dataList;

    }

    @Override
    public int getCount() {
        return UsersInfoList.size();
    }

    @Override
    public Object getItem(int i) {
        return UsersInfoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = View.inflate(context, R.layout.user_item, null );

        TextView t_name = (TextView) v.findViewById(R.id.name);
        TextView t_score1 = (TextView) v.findViewById(R.id.score1);
        TextView t_score2 = (TextView) v.findViewById(R.id.score2);

        t_name.setText(UsersInfoList.get(i).name);
        t_score1.setText(UsersInfoList.get(i).score1);
        t_score2.setText(UsersInfoList.get(i).score2);

        return v;
    }

}
