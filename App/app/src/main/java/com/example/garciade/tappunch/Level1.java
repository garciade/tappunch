package com.example.garciade.tappunch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Level1 extends AppCompatActivity {

    int needed_taps = 25;
    int taps = 0;

    TextView timerText;
    CountDownTimer timer;
    long remaining_time = 0;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    /**
     * Creates the activity, sets the click listeners and updates the shared preferences
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1);

        preferences = PreferenceManager.getDefaultSharedPreferences(Level1.this);
        editor = preferences.edit();

        View myView = findViewById(android.R.id.content);
        myView.setOnTouchListener(handleTouch);

        timerText = (TextView)findViewById(R.id.timerText);

        ImageButton backButton = (ImageButton)findViewById(R.id.back_button);

        backButton.setOnClickListener( new View.OnClickListener() {

            public void onClick(View view) {

                timer.cancel();

                Intent myIntent = new Intent(Level1.this, Menu.class);
                Level1.this.startActivity(myIntent);

            }
        });

        timer = new CountDownTimer(20*1000, 1000){

            @Override
            public void onTick(long l) {

                timerText.setText("" + l/1000);
                remaining_time = l / 1000;
            }
            @Override
            public void onFinish() {

                Intent intent = new Intent(Level1.this, LoseActivity.class);
                startActivity(intent);
            }
        }.start();
    }

    /**
     * Sets the events related to the screen touch during the game
     */
    private View.OnTouchListener handleTouch = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    Log.i("TAG", "touched down");
                    taps++;
                    break;
            }

            // Ended level
            if(taps >= needed_taps) {

                timer.cancel();

                int final_score = (taps * ((int) remaining_time))*122;

                preferences = PreferenceManager.getDefaultSharedPreferences(Level1.this);
                editor = preferences.edit();

                // Update persistent highscore
                if(final_score > preferences.getInt("highscore_level1", 0)){

                    //--SAVE Data
                    editor.putInt("highscore_level1", final_score);
                    editor.commit();

                }

                editor.putBoolean("level2unlocked", true);
                editor.commit();

                Intent intent = new Intent(Level1.this, WinActivity.class);
                Bundle b = new Bundle();
                b.putInt("score", final_score);
                intent.putExtras(b);
                startActivity(intent);
                finish();

            }
            return true;
        }
    };
}
