package com.example.garciade.tappunch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    // FIREBASE
    private FirebaseAnalytics mFirebaseAnalytics;
    private AdView mAdView;


    /**
     * Sets the shared preferences, firebase connection, the login logic and the button functionality
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        AnalyticsEvents.init(getApplication());

        // FIREBASE SETUP
        Fabric.with(this, new Crashlytics());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("message");
        myRef.setValue("Hello, World!");

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d("data change", "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("change failed", "Failed to read value.", error.toException());
            }
        });

        logEvent("0", "aitor", "none");

        // LOGIN BUTTON
        Button loginButton = (Button)findViewById(R.id.login_button);
        final EditText user_field = (EditText)findViewById(R.id.username_login);
        final EditText pass_field = (EditText)findViewById(R.id.password_login);

        String login_user = "aitor";
        String login_pass = "aitor";

        loginButton.setOnClickListener( new View.OnClickListener() {

            public void onClick(View view) {

                // login correct
                if((user_field.getText().toString().compareTo(login_user) == 0) && (pass_field.getText().toString().compareTo(login_pass) == 0)) {

                    Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(MainActivity.this, Menu.class);
                    Bundle b = new Bundle();
                    int score1 = preferences.getInt("score1", 0);
                    b.putInt("highscore_level1", score1);
                    intent.putExtras(b);
                    startActivity(intent);
                    finish();

                    AnalyticsEvents.notifyEvent("1", "login", "info");

                    Intent myIntent = new Intent(MainActivity.this, Menu.class);
                    MainActivity.this.startActivity(myIntent);
                }
                // login incorrect
                else{
                    if(user_field.getText().toString().compareTo(login_user) == 0){
                        Toast.makeText(getApplicationContext(), "Incorrect password", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Invalid username", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    protected void logEvent(String id, String name, String image) {

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, image);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

}
