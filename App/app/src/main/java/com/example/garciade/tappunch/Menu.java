package com.example.garciade.tappunch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Menu extends AppCompatActivity {

    int highscore1 = 0;
    int highscore2 = 0;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    /**
     * Sets the buttons functionality of the menu activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        preferences = PreferenceManager.getDefaultSharedPreferences(Menu.this);
        editor = preferences.edit();

        // SET HIGHSCORE TEXTS
        highscore1 = preferences.getInt("highscore_level1", 0);
        TextView highscore1_text = (TextView)findViewById(R.id.highscore1);
        highscore1_text.setText(String.valueOf(highscore1));
        highscore2 = preferences.getInt("highscore_level2", 0);
        TextView highscore2_text = (TextView)findViewById(R.id.highscore2);
        highscore2_text.setText(String.valueOf(highscore2));


        // START LEVEL 1
        ImageButton level1 = (ImageButton)findViewById(R.id.level1);
        level1.setOnClickListener( new View.OnClickListener() {

            public void onClick(View view) {

                Intent myIntent = new Intent(Menu.this, Level1.class);
                startActivity(myIntent);

            }
        });

        // CHECK IF GET LOCKED LEVEL
        ImageView lock = (ImageView)findViewById(R.id.lock);
        ImageButton level2 = (ImageButton)findViewById(R.id.level2);
        if(preferences.getBoolean("level2unlocked", false)){

            lock.setVisibility(View.INVISIBLE);
            level2.setOnClickListener( new View.OnClickListener() {

                public void onClick(View view) {
                    Intent myIntent = new Intent(Menu.this, Level2.class);
                    startActivity(myIntent);
                }

            });
        }


        //GO TO RANKING
        ImageButton rankingButton = (ImageButton)findViewById(R.id.rankingButton);
        rankingButton.setOnClickListener( new View.OnClickListener() {

            public void onClick(View view) {

                Intent myIntent = new Intent(Menu.this, FragmentRankingActivity.class);
                startActivity(myIntent);

            }
        });

        // GO TO CONTACT
        ImageButton contactButton = (ImageButton)findViewById(R.id.contactButton);
        contactButton.setOnClickListener( new View.OnClickListener() {

            public void onClick(View view) {

                Intent myIntent = new Intent(Menu.this, Contact.class);
                startActivity(myIntent);

            }
        });
    }
}