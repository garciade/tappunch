package com.example.garciade.tappunch;

/**
 * Created by Aitor on 27/05/2018.
 */

public class UserInfo {

    String name;
    String score1;
    String score2;

    /**
     * Users info constructor
     * @param name
     * @param score1
     * @param score2
     */
    public UserInfo(String name, String score1, String score2){

        this.name = name;
        this.score1 = score1;
        this.score2 = score2;

    }


}
