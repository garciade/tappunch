package com.example.garciade.tappunch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class WinActivity extends AppCompatActivity {

    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win);

        Bundle b = getIntent().getExtras();
        if(b != null) {
            score = b.getInt("score");
            TextView score_text = (TextView)findViewById(R.id.score);
            score_text.setText(String.valueOf(score));
        }

        Button backButton = (Button)findViewById(R.id.menuButton);
        backButton.setOnClickListener( new View.OnClickListener() {

            public void onClick(View view) {

                Intent intent = new Intent(WinActivity.this, Menu.class);
                startActivity(intent);

            }
        });
    }
}